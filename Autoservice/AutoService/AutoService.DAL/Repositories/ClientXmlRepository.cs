﻿using AutoService.DAL.Model;
using AutoService.DAL.Repositories.Interface;
using AutoService.DAL.Serializer;
using System.Collections.Generic;

namespace AutoService.DAL.Repositories
{
    public class ClientXmlRepository : IClientXmlRepository
    {
        public List<Client> ReadAll(string fileRoute)
        {
            return XMLSerializer<List<Client>>.Deserialize(fileRoute, typeof(Client));
        }

        public void WrightAll(List<Client> clients, string fileRoute)
        {
            XMLSerializer<List<Client>>.Serialize(clients, fileRoute, typeof(Client));
        }
    }
}
