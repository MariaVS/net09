﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Task10_1.Fractions
{
    class FractionAlphabeticalComparer : IComparer<Fraction>
    {
        public int Compare( Fraction x, Fraction y)
        {
            string str1 = x.ToString(FractionPresentationType.Decimale);
            string str2 = y.ToString(FractionPresentationType.Decimale);
            return string.Compare(str1, str2);
        }
    }
}
