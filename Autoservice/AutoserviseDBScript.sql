USE [TestBase]
GO
/****** Object:  Table [dbo].[Cars]    Script Date: 03.05.2021 23:00:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cars](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Brand] [nvarchar](50) NOT NULL,
	[YearOfIssue] [int] NOT NULL,
	[CudtomerId] [int] NOT NULL,
 CONSTRAINT [PK_Cars] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 03.05.2021 23:00:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](max) NOT NULL,
	[LastName] [nvarchar](max) NOT NULL,
	[DayOfBirth] [datetime] NOT NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Maintenances]    Script Date: 03.05.2021 23:00:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Maintenances](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CarId] [int] NOT NULL,
	[TaskId] [int] NOT NULL,
	[ServiseDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Maintenances] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Persons]    Script Date: 03.05.2021 23:00:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Persons](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FistName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[DateOfBith] [datetime] NOT NULL,
 CONSTRAINT [PK_Person] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PersonsTasks]    Script Date: 03.05.2021 23:00:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonsTasks](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PersonId] [int] NOT NULL,
	[TaskId] [int] NOT NULL,
 CONSTRAINT [PK_PersonsTasks] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tasks]    Script Date: 03.05.2021 23:00:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tasks](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Tasks] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Cars] ON 
GO
INSERT [dbo].[Cars] ([Id], [Brand], [YearOfIssue], [CudtomerId]) VALUES (1, N'BMW', 2019, 3)
GO
INSERT [dbo].[Cars] ([Id], [Brand], [YearOfIssue], [CudtomerId]) VALUES (2, N'Ferrari', 2017, 2)
GO
INSERT [dbo].[Cars] ([Id], [Brand], [YearOfIssue], [CudtomerId]) VALUES (3, N'Mercedes', 2021, 1)
GO
SET IDENTITY_INSERT [dbo].[Cars] OFF
GO
SET IDENTITY_INSERT [dbo].[Customers] ON 
GO
INSERT [dbo].[Customers] ([Id], [FirstName], [LastName], [DayOfBirth]) VALUES (1, N'Teatuev', N'Vladislav', CAST(N'1995-04-01T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Customers] ([Id], [FirstName], [LastName], [DayOfBirth]) VALUES (2, N'Daria', N'Shwarz', CAST(N'1876-09-23T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Customers] ([Id], [FirstName], [LastName], [DayOfBirth]) VALUES (3, N'Diana', N'gladkaia', CAST(N'1994-05-25T00:00:00.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Customers] OFF
GO
SET IDENTITY_INSERT [dbo].[Maintenances] ON 
GO
INSERT [dbo].[Maintenances] ([Id], [CarId], [TaskId], [ServiseDate]) VALUES (1, 2, 1, CAST(N'2021-12-03T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Maintenances] ([Id], [CarId], [TaskId], [ServiseDate]) VALUES (2, 1, 3, CAST(N'2021-10-02T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Maintenances] ([Id], [CarId], [TaskId], [ServiseDate]) VALUES (3, 3, 2, CAST(N'2021-05-09T00:00:00.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Maintenances] OFF
GO
SET IDENTITY_INSERT [dbo].[Persons] ON 
GO
INSERT [dbo].[Persons] ([Id], [FistName], [LastName], [DateOfBith]) VALUES (3, N'Maria ', N'Vasilieva', CAST(N'2002-07-20T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Persons] ([Id], [FistName], [LastName], [DateOfBith]) VALUES (5, N'Ira', N'Tetueva', CAST(N'1994-09-01T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Persons] ([Id], [FistName], [LastName], [DateOfBith]) VALUES (6, N'Mikail', N'White', CAST(N'1980-09-08T00:00:00.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Persons] OFF
GO
SET IDENTITY_INSERT [dbo].[PersonsTasks] ON 
GO
INSERT [dbo].[PersonsTasks] ([Id], [PersonId], [TaskId]) VALUES (6, 3, 1)
GO
INSERT [dbo].[PersonsTasks] ([Id], [PersonId], [TaskId]) VALUES (7, 6, 3)
GO
INSERT [dbo].[PersonsTasks] ([Id], [PersonId], [TaskId]) VALUES (8, 5, 2)
GO
SET IDENTITY_INSERT [dbo].[PersonsTasks] OFF
GO
SET IDENTITY_INSERT [dbo].[Tasks] ON 
GO
INSERT [dbo].[Tasks] ([Id], [Description]) VALUES (1, N'rubber replacement')
GO
INSERT [dbo].[Tasks] ([Id], [Description]) VALUES (2, N'Repairing the engine')
GO
INSERT [dbo].[Tasks] ([Id], [Description]) VALUES (3, N'windshield replacement')
GO
SET IDENTITY_INSERT [dbo].[Tasks] OFF
GO
ALTER TABLE [dbo].[Cars]  WITH CHECK ADD  CONSTRAINT [FK_Cars_Customers] FOREIGN KEY([CudtomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[Cars] CHECK CONSTRAINT [FK_Cars_Customers]
GO
ALTER TABLE [dbo].[Maintenances]  WITH CHECK ADD  CONSTRAINT [FK_Maintenances_Cars] FOREIGN KEY([CarId])
REFERENCES [dbo].[Cars] ([Id])
GO
ALTER TABLE [dbo].[Maintenances] CHECK CONSTRAINT [FK_Maintenances_Cars]
GO
ALTER TABLE [dbo].[Maintenances]  WITH CHECK ADD  CONSTRAINT [FK_Maintenances_Tasks] FOREIGN KEY([TaskId])
REFERENCES [dbo].[Tasks] ([Id])
GO
ALTER TABLE [dbo].[Maintenances] CHECK CONSTRAINT [FK_Maintenances_Tasks]
GO
ALTER TABLE [dbo].[PersonsTasks]  WITH CHECK ADD  CONSTRAINT [FK_PersonsTasks_Persons] FOREIGN KEY([PersonId])
REFERENCES [dbo].[Persons] ([Id])
GO
ALTER TABLE [dbo].[PersonsTasks] CHECK CONSTRAINT [FK_PersonsTasks_Persons]
GO
ALTER TABLE [dbo].[PersonsTasks]  WITH CHECK ADD  CONSTRAINT [FK_PersonsTasks_Tasks] FOREIGN KEY([TaskId])
REFERENCES [dbo].[Tasks] ([Id])
GO
ALTER TABLE [dbo].[PersonsTasks] CHECK CONSTRAINT [FK_PersonsTasks_Tasks]
GO
