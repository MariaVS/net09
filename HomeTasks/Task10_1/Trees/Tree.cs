﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task10_1.Trees
{
    public class Tree
    {
        private List<TreeItemLevel<int>> _list = new List<TreeItemLevel<int>>();
        public TreeItem<int> Head { get; set; }
        public Tree(int value)
        {
            Head = new TreeItem<int>(value);
        }

        public TreeItem<int> Search(TreeItem<int> current, int key)
        {
            if ( current== null || key == current.Value)
            {
                return current;
            }

            if (key < current.Value)
            {
                return Search(current.Left, key);
            }

            return Search(current.Right, key);
        }

        public TreeItem<int> Search(int key)
        {
            return Search(Head, key);
        }
        public TreeItem<int> Add(int key)
        {
            return Add(Head, key);
        }
        public TreeItem<int> Add(TreeItem<int> current, int key)
        {
            if (current == null)
            {
                return new TreeItem<int>(key);
            }
            else if (key < current.Value)
            {

                current.Left = Add(current.Left, key);
                current.Left.Parent = current;
            }
            else if (key > current.Value)
            {
                current.Right = Add(current.Right, key);
                current.Right.Parent = current;
            }

            return current;
        }

        public TreeItem<int> Update(int find, TreeItem<int> current, int update)
        {
            var result = Search(current, find);
            if (result is { })
            {
                result.Value = update;
            }
            return result;
        }

        public TreeItem<int> Minimum(TreeItem<int> current)
        {

            if (current.Left == null)
            {
                return current;
            }

            return Minimum(current.Left);
        }

        public TreeItem<int> Delete(TreeItem<int> current, int key)
        {
            if (current == null)
            {
                return current;
            }
            if (key < current.Value)
            {
                current.Left = Delete(current.Left, key);
            }
            else if (key > current.Value)
            {
                current.Right = Delete(current.Right, key);
            }
            else if (current.Left != null && current.Right != null)
            {
                current.Value = Minimum(current.Right).Value;
                current.Right = Delete(current.Right, current.Value);
            }
            else
            {
                if (current.Left != null)
                {
                    current = current.Left;
                }
                else if (current.Right != null)
                {
                    current = current.Right;
                }
                else
                {
                    current = null;
                }
            }

            return current;
        }
        
         
        public void InorderTraversal(TreeItem<int> current, int counter = 1)
        {
            if (current != null)
            {
                _list.Add(new TreeItemLevel<int>() {Level = counter, TreeItem = current});
                counter++;
                InorderTraversal(current.Left, counter);
                InorderTraversal(current.Right, counter);

            }
        }

        public void InorderTraversal(int counter = 1)
        {
            InorderTraversal(Head, counter);
        }
        public void DrawToConsole(int counter = 1)
        {
            InorderTraversal(counter);
            var maxLevel = _list.Max(x => x.Level);
            var resultMaxItemsOnMaxLevel = CalculateMaxLevelItemsCount(maxLevel);
            var maxLength = resultMaxItemsOnMaxLevel * 3 + (resultMaxItemsOnMaxLevel - 1) * 2;
            DrawItem(Head, Console.CursorTop, maxLength, 1, true, maxLength);
        }

        public void DrawItem(TreeItem<int> item, int y, int parentPosition, int level, bool isLeft, int maxLength)
        {
            if (item is null)
            {
                return;
            }
            var halfParent = parentPosition / 2;
            int itemPosition;
            if (isLeft)
            {
                itemPosition = halfParent;
            }
            else
            {
                itemPosition = parentPosition + halfParent;
            }
            Console.SetCursorPosition(itemPosition, y);
            Console.Write(item.Value);
            //DrawItem(item.Left, y+1, halfLine, level+1, true, maxLength);
            //DrawItem(item.Right, y+1, halfLine + itemPosition, level+1, false, maxLength);
        }

        public int CalculateMaxLevelItemsCount(int level)
        {
            var resultCounter = 1;
            for (int i = 1; i < level; i++)
            {
                resultCounter *= 2;
            }

            return resultCounter;
        }

        public int GetItemPosition(int maxLength, int level)
        {
            int drawPosition = maxLength;
            for (int i = 0; i < level; i++)
            {
                drawPosition = drawPosition / 2;
            }
            return drawPosition;
        }
    }  
}
