﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AutoService.DAL.Model
{
    public class Task
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [MaxLength(50)]
        [Column(TypeName = "text")]
        public string Description { get; set; }
        
        [MaxLength(50)]
        public string Title { get; set; }
        [Required]
        public bool IsCompleted { get; set; }
        [Required]
        public bool IsInWork { get; set; }
        [Required]
        public bool IsPending { get; set; }
        [Required]
        public int TaskPriorityId { get; set; }
        public virtual ICollection<Person> Persons { get; set; }
        public int OrderId { get; set; }
        [ForeignKey("OrderId")]
        public virtual Order Order { get; set; }
        public int? ParentId { get; set; }
        public virtual ICollection<Task> Children { get; set; }
        [ForeignKey("ParentId")]
        public virtual Task Parent { get; set; }

    }
}
