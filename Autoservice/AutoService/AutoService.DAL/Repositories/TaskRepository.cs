﻿using AutoService.DAL.Model;

namespace AutoService.DAL.Repositories
{
    public class TaskRepository : GenericRepository<Task>
    {
        public TaskRepository (AutoDbContext context) : base(context) { }
        public override void Delete(Task entity)
        {
            var taskArchive = new TaskArchive(entity);
            Context.Add(taskArchive);
            Context.Remove(entity);
            Context.SaveChanges();
        }
    }
}
