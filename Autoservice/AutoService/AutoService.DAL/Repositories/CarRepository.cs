﻿using System.Linq;
using AutoService.DAL.Model;
using AutoService.DAL.Repositories.Interface;

namespace AutoService.DAL.Repositories
{
    public class CarRepository : GenericRepository<Car>, ICarsRepository
    {
        public CarRepository(AutoDbContext context) : base(context) { }

        public IQueryable<Car> ReadAllByClientId(int clientId)
        {
            return Context.Set<Car>().Where( car => car.ClientId == clientId);
        }
    }
}
