﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Task10_1.Tanks
{
    public class Army : IEnumerable<Tank>
    {
        List<Tank> _tanks = new List<Tank>();
        private EnumForEnumerator _enumForEnumerator;
        public Army(List<Tank> tanks, EnumForEnumerator enumForEnumerator)
        {
            _tanks = tanks;
            _enumForEnumerator = enumForEnumerator;
        }

        public IEnumerator<Tank> GetEnumerator()
        {
            if (_enumForEnumerator == EnumForEnumerator.RandomEnumerator)
            {
                return new TankRandomEnumerator(_tanks);
            }

            return new TankRiseEnumerator(_tanks);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            if (_enumForEnumerator == EnumForEnumerator.RiseEnumerator)
            {
                return new TankRiseEnumerator(_tanks);
            }
            return new TankRandomEnumerator(_tanks);
        }
    }
}
