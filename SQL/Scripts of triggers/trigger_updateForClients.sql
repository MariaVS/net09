use [TestBase]
go
CREATE OR ALTER TRIGGER Clients_UPDATE 
   ON [dbo].[Clients] 
   AFTER UPDATE
AS
INSERT INTO Clients_Audits 
SELECT d.DataOfUpd
      ,d.LastName
      ,d.FirstName
      ,d.Number
      ,d.Email
      ,i.DataOfUpd
      ,i.LastName
      ,i.LastName
      ,i.Number
      ,i.Email
FROM DELETED d JOIN INSERTED i
ON d.Id = i.Id

