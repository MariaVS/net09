﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task10_1.Figures
{
    public abstract class Figure
    {
        public abstract void Draw();
    }
}
