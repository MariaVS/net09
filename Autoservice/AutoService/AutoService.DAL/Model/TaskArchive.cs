﻿namespace AutoService.DAL.Model
{
    public class TaskArchive
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsInWork { get; set; }
        public bool IsPending { get; set; }
        public int TaskPriorityId { get; set; }
        public int OrderId { get; set; }
        public int? ParentId { get; set; }
        public virtual Task Parent { get; set; }

        public TaskArchive() { }
        public TaskArchive(Task task)
        {
            Description = task.Description;
            Title = task.Title;
            IsCompleted = task.IsCompleted;
            IsInWork = task.IsInWork;
            IsPending = task.IsPending;
            TaskPriorityId = task.TaskPriorityId;
            OrderId = task.OrderId;
            ParentId = task.ParentId;
        }
    }
}
