﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task10_1.Trees
{
    public class TreeItem<T>
    {
        public T Value { get; set; }
        public TreeItem<T> Left { get; set; }
        public TreeItem<T> Right { get; set; }
        public TreeItem<T> Parent { get; set; }
        public TreeItem(T value)
        {
            this.Value = value;
        }
    }
}
