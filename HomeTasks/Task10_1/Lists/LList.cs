﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Task10_1.Lists
{
    class LList<T>:IEnumerable<T> where T: class
    {
        public ListItem<T> Head { get; set; }


        public LList(T value)
        {
            Head = new ListItem<T>(value);
        }

        public void Add(T value)
        {
            var last = Head;
            while (last.Next is { })
            {
                last = last.Next;
            }
            last.Next = new ListItem<T>(value);
        }

        public void Add(ListItem<T> root, T value)
        {
            if (root.Next is null)
            {
                root.Next = new ListItem<T>(value);
                return;
            }
            else
            {
                Add(root.Next, value);
            }
        }
        public ListItem<T> FindReq(T find, ListItem<T> current)
        {
            if (current.Value.Equals(find))
            {
                return current;
            }
            if (current.Next is null)
            {
                return null;
            }
            else
            {
                return FindReq(find, current.Next);
            }

        }

        public ListItem<T> UpdateReq(T find, ListItem<T> current, T update)
        {
            var result = FindReq(find, current);
            if ( result is { })
            {
                result.Value = update;
            }
            return result;
        }

        public void Remove(ListItem<T> root, T find)
        {
            var current = root;
            if (current.Next is null)
            {
                return;
            }
            else if (current.Next.Value == find)
            {
                current.Next = current.Next.Next;
            }
            Remove(root.Next, find);
        }

        public IEnumerator<T> GetEnumerator()
        {
            var current = Head;
            while (current is { })
            {
                yield return current.Value;
                current = current.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            var current = Head;
            while (current is { })
            {
                yield return current.Value;
                current = current.Next;
            }
        }
    }
}
