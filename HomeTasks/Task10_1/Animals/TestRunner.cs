﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task10_1.Animals
{
    public class TestRunner
    {
        public static void Test()
        {
            List<Animal> animal = new List<Animal>() { new Parrot(), new Cow(), new Parrot(), new Cow() };
            foreach (var an in animal)
            {
                an.Say();
            }
            List<Herd> list = new List<Herd>() {new Sheep(), new Wolf(), new Sheep(), new Sheep()};
            foreach (var lis in list)
            {
                
                if (lis is Wolf)
                {
                    Wolf wolf = lis as Wolf;
                    wolf.Say();
                }
                lis.Say();

            }
        }
        
    }
}
