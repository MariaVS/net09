﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AutoService.DAL.Migrations
{
    public partial class fixedPluralKeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PersonTask_Persons_PersonId",
                table: "PersonTask");

            migrationBuilder.DropForeignKey(
                name: "FK_PersonTask_Tasks_TaskId",
                table: "PersonTask");

            migrationBuilder.RenameColumn(
                name: "TaskId",
                table: "PersonTask",
                newName: "TasksId");

            migrationBuilder.RenameColumn(
                name: "PersonId",
                table: "PersonTask",
                newName: "PersonsId");

            migrationBuilder.RenameIndex(
                name: "IX_PersonTask_TaskId",
                table: "PersonTask",
                newName: "IX_PersonTask_TasksId");

            migrationBuilder.AddForeignKey(
                name: "FK_PersonTask_Persons_PersonsId",
                table: "PersonTask",
                column: "PersonsId",
                principalTable: "Persons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PersonTask_Tasks_TasksId",
                table: "PersonTask",
                column: "TasksId",
                principalTable: "Tasks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PersonTask_Persons_PersonsId",
                table: "PersonTask");

            migrationBuilder.DropForeignKey(
                name: "FK_PersonTask_Tasks_TasksId",
                table: "PersonTask");

            migrationBuilder.RenameColumn(
                name: "TasksId",
                table: "PersonTask",
                newName: "TaskId");

            migrationBuilder.RenameColumn(
                name: "PersonsId",
                table: "PersonTask",
                newName: "PersonId");

            migrationBuilder.RenameIndex(
                name: "IX_PersonTask_TasksId",
                table: "PersonTask",
                newName: "IX_PersonTask_TaskId");

            migrationBuilder.AddForeignKey(
                name: "FK_PersonTask_Persons_PersonId",
                table: "PersonTask",
                column: "PersonId",
                principalTable: "Persons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PersonTask_Tasks_TaskId",
                table: "PersonTask",
                column: "TaskId",
                principalTable: "Tasks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
