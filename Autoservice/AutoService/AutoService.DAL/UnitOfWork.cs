﻿using System;
using AutoService.DAL.Model;

namespace AutoService.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        protected readonly AutoDbContext Context;
        private Lazy<TaskRepository> _taskRepository;
        public TaskRepository TaskRepository => (_taskRepository ??= new Lazy<TaskRepository>(() => new TaskRepository(Context))).Value;

        private Lazy<CarRepository> _carRepository;
        public CarRepository CarRepository => (_carRepository ??= new Lazy<CarRepository>(() => new CarRepository(Context))).Value;

        private Lazy<PersonRepo> _personRepo;
        public PersonRepo PersonRepository => (_personRepo ??= new Lazy<PersonRepo>(() => new PersonRepo(Context))).Value;

        private Lazy<GenericRepository<TaskArchive>> _taskArchiveRepository;
        public GenericRepository<TaskArchive> TaskArchiveRepository => (_taskArchiveRepository ??=new Lazy<GenericRepository<TaskArchive>>(() => new GenericRepository<TaskArchive>(Context))).Value;

        private Lazy<GenericRepository<Client>> _clientRepository;
        public GenericRepository<Client> ClientRepository => (_clientRepository ??= new Lazy<GenericRepository<Client>>(() => new GenericRepository<Client>(Context))).Value;
        public UnitOfWork(AutoDbContext context)
        {
            Context = context;
        }

        public IGenericRepository<T> GetGenericRepository<T>() where T : class
        {
            return new GenericRepository<T>(Context);
        }
    }
}
