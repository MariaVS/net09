﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualBasic;

namespace Task10_1.Tanks
{
    public class Tank
    {
        public string Name { get; set; }
        public int Damage { get; set; }
        public int Armor { get; set; }
        public int Wins { get; set; }

        public static bool operator ==(Tank с1, Tank с2)
        {
            return с1.Damage + с1.Armor == с2.Damage + с2.Armor;
        }
        public static bool operator !=(Tank с1, Tank с2)
        {
            return (!(с1 == с2));
        }
        public static bool operator >(Tank c1, Tank c2)
        {
            return c1.Damage + c1.Armor > c2.Damage + c2.Armor;
        }
        public static bool operator <(Tank c1, Tank c2)
        {
            return c1.Damage + c1.Armor < c2.Damage + c2.Armor;
        }

        public static void Multiply(Tank c1, Tank c2)
        {
            if (c1 == c2)
            {
                return;
            } 
            if (c1 > c2)
            {
                c1.Wins++;
                return;
            }
            c2.Wins++;
        }
        public override bool Equals(object o)
        {
            if (o == null || o.GetType() != GetType())
                return false;
            Tank f = (Tank)o;
            return (this == f);
        }
        public override int GetHashCode()
        {
            return (Damage ^ Armor);
        }

    }
}
