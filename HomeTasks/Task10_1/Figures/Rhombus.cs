﻿using System;

namespace Task10_1.Figures
{
    class Rhombus : Figure
    {
        public int CrossVertical { get; set; }
        public int CrossHorizontal { get; set; }
        public Rhombus() { }

        public Rhombus(int crossVertical, int crossHorizontal)
        {
            CrossVertical = crossVertical;
            CrossHorizontal = crossHorizontal;
        }
        public override void Draw()
        {
            var isInverse = CrossVertical > CrossHorizontal;
            var startX = Console.CursorLeft;
            var startY = Console.CursorTop;
            var upX = CrossHorizontal / 2;
            var upY = Console.CursorTop;
            var rightX = CrossHorizontal;
            var rightY = Console.CursorTop + CrossVertical / 2;
            if (isInverse)
            {
                DrawLine(upY, rightY, upX, rightX, isInverse);
            }
            else
            {
                DrawLine(upX, rightX, upY, rightY, isInverse);
            }

            Console.CursorLeft = startX;
            Console.CursorTop = startY;

            var leftX = 0;
            var leftY = Console.CursorTop + CrossVertical / 2;
            if (isInverse)
            {
                DrawLine(upY, leftY, upX, leftX, isInverse);
            }
            else
            {
                DrawLine(leftX, upX, leftY, upY, isInverse);
            }

            Console.CursorLeft = startX;
            Console.CursorTop = leftY;

            var botX = upX;
            var botY = upY + CrossVertical;
            if (isInverse)
            {
                DrawLine(leftY, botY, leftX, botX, isInverse);
            }
            else
            {
                DrawLine(leftX, botX, leftY, botY, isInverse);
            }
            
            startX = 0;
            startY = Console.CursorTop + 1;
            if (isInverse)
            {
                DrawLine(rightY, botY, rightX, botX, isInverse);
            }
            else
            {
                DrawLine(botX, rightX, botY, rightY, isInverse);
            }

            Console.SetCursorPosition(startX, startY);
        }

        private void DrawLine(int x0, int x1, int y0, int y1, bool isInverse)
        {
            int deltax = Math.Abs(x1 - x0);
            int deltay = Math.Abs(y1 - y0);
            int error = 0;
            int deltaerr = (deltay + 1);
            int y = y0;
            int diry = y1 - y0;
            if (diry > 0)
            {
                diry = 1;
            }
            if (diry < 0)
            {
                diry = -1;
            }

            for (int x = x0; x <= x1; x++)
            {
                if (isInverse)
                {
                    Console.SetCursorPosition(2*y, x);
                }
                else
                {
                    Console.SetCursorPosition(2*x, y);
                }
                Console.Write("*");
                error = error + deltaerr;
                if (error >= (deltax + 1))
                {
                    y = y + diry;
                    error = error - (deltax + 1);
                }
            }
        }
    }
}
