﻿using System;
using System.Threading;

namespace Task10_1.Fractions
{
    public class Fraction : IEquatable<Fraction>, IComparable<Fraction>
    {

        public Action<int> Onchange;

        private int _num;
        private int _den;

        public int Num
        {
            get { return _num;}
            set
            {
                Onchange?.Invoke(value);
                _num = value;
            }

        }

        public int Den
        {
            get { return _den; }
            set
            {
                Onchange?.Invoke(value);
                _den = value;
            }

        }

        public Fraction(int num, int den)
        {
            if (den == 0)
                throw new DivideByZeroException();
            Num = num;
            Den = den;
        }
         
        public Fraction() {}
        public static Fraction operator +(Fraction c1, Fraction c2)
        {
            int r1 = c1.Num * c2.Den + c2.Num * c1.Den;
            int r2 = c1.Den * c2.Den;
            return new Fraction(r1, r2);
        }
        public static Fraction operator -(Fraction c1, Fraction c2)
        {
            int r1 = c1.Num * c2.Den - c2.Num * c1.Den;
            int  r2 = c1.Den * c2.Den;
            return new Fraction(r1, r2);
        }
        public static Fraction operator *(Fraction c1, Fraction c2)
        {
            int r1 = c1.Num * c2.Num;
            int r2 = c1.Den * c2.Den;
            return new Fraction(r1, r2);
        }
        public static Fraction operator /(Fraction c1, Fraction c2)
        {
            int r1 = c1.Num * c2.Den;
            int r2 = c1.Den * c2.Num;
            return new Fraction(r1, r2);
        }

        public static bool operator ==(Fraction с1, Fraction с2)
        {
            return с1.Num * с2.Den == с2.Num * с1.Den;
        }

        public static bool operator !=(Fraction с1, Fraction с2)
        {
            return (!(с1 == с2));
        }


        public static bool operator >(Fraction c1, Fraction c2)
        {
            return c1.Num * c2.Den > c2.Num * c1.Den;
        }

        public static bool operator >=(Fraction c1, Fraction c2)
        {
            return (!(c1 < c2));
        }

        public static bool operator <(Fraction c1, Fraction c2)
        {
            return c1.Num * c2.Den < c2.Num * c1.Den;
        }

        public static bool operator <=(Fraction c1, Fraction c2)
        {
            return (!(c1 > c2));
        }
        public override bool Equals(object o)
        {
            return EqualsFraction(o);
        }

        public override int GetHashCode()
        {
            return (Num ^ Den);
        }

        public override string ToString()
        {
            if (Den == 1)
                return Num.ToString();
            else
                return Num + "/" + Den;
        }
        public string ToString(FractionPresentationType presentation)
        {
            if (presentation == FractionPresentationType.Fraction)
            {
                return $"{this.Num}/{this.Den}";
            }
            return $"{(double)this}";
        }

        public bool Equals(Fraction other)
        {
            return EqualsFraction(other);
        }

        public static implicit operator double(Fraction f)
        {
            return (double)f.Num / (double)f.Den;
        }
        public static implicit operator float(Fraction f)
        {
            return (float)f.Num / (float)f.Den;
        }

        public Fraction Inverse()
        {
            return new Fraction(this.Den, this.Num);
        }
        public bool EqualsFraction(object o)
        {
            if (o == null || o.GetType() != GetType())
                return false;
            Fraction f = (Fraction)o;
            return (this == f);
        }

        public int CompareTo(Fraction other)
        {
            if (EqualsFraction(other))
            {
                return 0;
            }

            Fraction f = other;
            if (this > f)
            {
                return 1;
            }

            return -1;
        }

        public double this[int index] 
        {
            get
            {
                double db = this;
                string str = db.ToString();
                int comePosition = str.IndexOf(".");
                return comePosition + index;
            }
        }
    }
}
