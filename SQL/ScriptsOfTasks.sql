USE TestBase
-- ������� ��� ������, � ������� ������ ������ �����������

SELECT t.Id, t.Description
FROM
(SELECT TaskId
FROM PersonsTasks pt
GROUP BY TaskId 
HAVING COUNT(*) > 1) AS res  
JOIN Tasks t ON t.Id = res.TaskId 




-- ������� ������������, � ������� ��� �����

SELECT p.*
FROM Persons p
LEFT JOIN PersonsTasks pt ON pt.PersonId = p.Id
WHERE pt.TaskId is NULL




-- ������� ��������, � �������� ������ ����� ����� (�� �� 0)

SELECT p.*, res.Count
FROM
	(SELECT MIN(res.Count) AS [MinCount]
	FROM
	(SELECT PersonId, COUNT(*) AS [Count]
	FROM PersonsTasks
	GROUP BY PersonId) AS res) AS minRes
		JOIN 
	(SELECT PersonId, COUNT(*) AS [Count]
	FROM PersonsTasks
	GROUP BY PersonId) AS res ON res.Count = minRes.MinCount
		JOIN Persons p ON p.Id = res.PersonId 




--������� ���� �����, � ���� ���� ������, ����������� �� ���������� �����

SELECT p.*, res.Count
FROM 
(SELECT PersonId, COUNT(*) AS [Count]
FROM PersonsTasks pt
GROUP BY pt.PersonId ) AS res
JOIN Persons p ON p.Id = res.PersonId
ORDER BY res.Count




--������� ��� ������, � ������� ���� �����������, ����������� �� ������� �����������

SELECT t.*, p.LastName
FROM Tasks t
JOIN PersonsTasks pt ON pt.TaskId = t.Id
JOIN Persons p ON p.Id = pt.PersonId
ORDER BY p.LastName




--������� ��� ������ �������� ������ � ������� ��� �����������

SELECT t.*
FROM Tasks t
LEFT JOIN PersonsTasks pt ON pt.TaskId = t.Id
WHERE t.ParentId is NULL AND pt.PersonId IS NULL




--������� ��� ������ 2��� ������

SELECT *
FROM Tasks t
WHERE t.ParentId IS NOT NULL
