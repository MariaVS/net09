use [TestBase]
go
CREATE OR ALTER TRIGGER [dbo].[Clients_DELETE] 
   ON [Clients] 
   INSTEAD OF DELETE
AS
UPDATE Clients 
SET IsDeleted = 1 
WHERE Id IN (select Id from DELETED)
