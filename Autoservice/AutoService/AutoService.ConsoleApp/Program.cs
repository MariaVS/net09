﻿using System.Diagnostics;
using System.IO;
using System.Linq;
using AutoService.BLL.Services;
using AutoService.DAL;
using AutoService.DAL.Model;
using AutoService.DAL.Repositories;
using AutoService.DAL.Repositories.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace AutoService.ConsoleApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();
            string connectionString = config.GetConnectionString("DefaultConnection");

            var optionsBuilder = new DbContextOptionsBuilder<AutoDbContext>();
            optionsBuilder.LogTo(message => Debug.WriteLine(message));
            var options = optionsBuilder.UseSqlServer(connectionString).Options;

            using (AutoDbContext ctx = new AutoDbContext(options))
            {
                //var autoServiceMenuRepository = new AutoServiceMenuRepository(ctx);
                //Menu menu = new Menu(autoServiceMenuRepository);
                //menu.ShowMainMenu();

                var clientRepo = new GenericRepository<Client>(ctx);
                var clients = clientRepo.ReadAll().Where(x => x.Id < 10).ToList();

                var xmlRoute = config.GetValue<string>("Application:ClientsXmlFileRoute");
                ClientXmlRepository clientXmlRepository = new ClientXmlRepository();
                ClientXmlService clientXmlService = new ClientXmlService(clientXmlRepository);
                foreach (var client in clients)
                {
                    clientXmlService.Add(client, xmlRoute);
                }

                var fileClients = clientXmlService.ReadAll(xmlRoute);
            }
        }
    }
}
