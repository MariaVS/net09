﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task10_1.Figures
{
    public class Circle : Figure
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Radius { get; set; }
        public Circle() { }

        public Circle(int x, int y, int radius)
        {
            X = x;
            Y = y;
            Radius = radius;
        }
        public override void Draw()
        {
            var cursorTop = Console.CursorTop;
            DrawQuadrant(X, Console.CursorTop + Y);
            Console.CursorLeft = 0;
            Console.CursorTop = cursorTop + 2*Radius + 1;
        }

        private void DrawQuadrant(int x0, int y0)
        {
            int x = 0;
            int y = Radius;
            int delta = 1 - 2 * Radius;
            int error = 0;
            while (y >= x)
            {
                DrawPixel(x0 + x, y0 + y);
                DrawPixel(x0 + x, y0 - y);
                DrawPixel(x0 - x, y0 + y);
                DrawPixel(x0 - x, y0 - y);
                DrawPixel(x0 + y, y0 + x);
                DrawPixel(x0 + y, y0 - x);
                DrawPixel(x0 - y, y0 + x);
                DrawPixel(x0 - y, y0 - x);
                error = 2 * (delta + y) - 1;
                if ((delta < 0) && (error <= 0))
                {
                    delta += 2 * ++x + 1;
                    continue;
                }

                if ((delta > 0) && (error > 0))
                {
                    delta -= 2 * --y + 1;
                    continue;
                }

                delta += 2 * (++x - --y);
            }
        }

        private void DrawPixel(int x, int y)
        {
            Console.SetCursorPosition(2 * x, y);
            Console.Write("*");
        }
    }
}
