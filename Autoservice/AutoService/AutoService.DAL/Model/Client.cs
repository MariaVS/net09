﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace AutoService.DAL.Model
{
    public class Client
    {
        public DateTime DataOfUpd { get; set; }
        public bool IsDeleted { get; set; }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [MaxLength(50)]
        public string LastName { get; set; }
        [MaxLength(50)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(20)]
        public string Number { get; set; }
        [MaxLength(50)]
        public string Email { get; set; }
        [XmlIgnore]
        public virtual ICollection<Order> Orders { get; set; } = new HashSet<Order>();
        [XmlIgnore]
        public virtual ICollection<Car> Cars { get; set; } = new HashSet<Car>();
    }
}
