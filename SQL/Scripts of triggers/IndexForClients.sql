USE [TestBase]
GO

SET ANSI_PADDING ON
GO

CREATE NONCLUSTERED INDEX [IX_Clients_LastName] ON [dbo].[Clients]
(
	[LastName] ASC
)
WHERE IsDeleted = 0
GO