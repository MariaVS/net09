﻿using System.Collections.Generic;
using System.Linq;
using AutoService.BLL.Services.Interfaces;
using AutoService.DAL.Model;
using AutoService.DAL.Repositories;

namespace AutoService.BLL.Services
{
    public class ClientService : IClientService
    {
        private IUnitOfWork _uow;

        public ClientService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public List<Client> GetAllClientsWithCars()
        {
            var clients = _uow.ClientRepository.ReadAll().ToList();
            foreach (var client in clients)
            {
                client.Cars = _uow.CarRepository.ReadAllByClientId(client.Id).ToList();
            }

            return clients;
        }
    }
}
