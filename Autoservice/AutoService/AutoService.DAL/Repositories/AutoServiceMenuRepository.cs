﻿using System.Collections.Generic;
using System.Linq;
using AutoService.DAL.Model;
using Microsoft.EntityFrameworkCore;

namespace AutoService.DAL.Repositories
{
    public class AutoServiceMenuRepository
    {
        private readonly AutoDbContext _ctx;
        public AutoServiceMenuRepository(AutoDbContext ctx)
        {
            _ctx = ctx;
        }

        public Car CreateCar(string model, string manufacture, string vin, string numberCar, int clientId)
        {
            var car = new Car
            {
                ClientId = clientId,
                Model = model,
                Manufacture = manufacture,
                VIN = vin,
                Number = numberCar
            };
            var createdCar = _ctx.Cars.Add(car).Entity;
            _ctx.SaveChanges();
            return createdCar;
        }

        public Client CreateClient(string firstName, string lastName, string numberClient, string email)
        {
            var client = new Client
            {
                FirstName = firstName,
                LastName = lastName,
                Number = numberClient,
                Email = email
            };
            var createdClient = _ctx.Clients.Add(client).Entity;
            _ctx.SaveChanges();
            return createdClient;
        }

        public void UpdateClient(Client client)
        {
            _ctx.Clients.Update(client);
            _ctx.SaveChanges();
        }

        public void RemoveClient(Client client)
        {
            _ctx.Clients.Remove(client);
            _ctx.SaveChanges();
        }

        public Client SearchClientById(int id)
        {
            return _ctx.Clients.Where(x => x.Id == id)
                .Include(x => x.Cars)
                .Include(x => x.Orders)
                .ThenInclude(x => x.Tasks)
                .FirstOrDefault();
        }

        public IList<Client> SearchClientByLastName(string lastName)
        {
            return _ctx.Clients.Where(x => x.LastName == lastName)
                .Include(x => x.Cars)
                .Include(x => x.Orders)
                .ThenInclude(x => x.Tasks).ToList();
        }
    }
}
