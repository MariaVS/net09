﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AutoService.DAL.Migrations
{
    public partial class TaskPrioritySeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "TaskPriorities",
                columns: new[] { "Id", "Priority" },
                values: new object[] { 1, "Minor" });

            migrationBuilder.InsertData(
                table: "TaskPriorities",
                columns: new[] { "Id", "Priority" },
                values: new object[] { 2, "Major" });

            migrationBuilder.InsertData(
                table: "TaskPriorities",
                columns: new[] { "Id", "Priority" },
                values: new object[] { 3, "Critical" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "TaskPriorities",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "TaskPriorities",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "TaskPriorities",
                keyColumn: "Id",
                keyValue: 3);
        }
    }
}
