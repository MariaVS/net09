﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task10_1.Figures
{
    class Rectangle : Figure
    {
        public int SideOne { get; set; }
        public int SideTwo { get; set; }

        public Rectangle() { }

        public Rectangle(int sideOne, int sideTwo)
        {
            SideOne = sideOne;
            SideTwo = sideTwo;
        }
        public override void Draw()
        {
            for (int i = 0; i < SideOne; i++)
            {
                Console.Write("* ");
            }
            Console.WriteLine();
            for (int i = 1; i < SideTwo-1; i++)
            {
                Console.Write("*");
                Console.SetCursorPosition(2 * (SideOne - 1), Console.CursorTop);
                Console.WriteLine("*");
            } 
            for (int i = 0; i < SideOne; i++)
            {
                Console.Write("* ");
            }
            Console.WriteLine();
        }
    }
}
