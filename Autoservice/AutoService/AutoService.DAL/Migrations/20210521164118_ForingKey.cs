﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AutoService.DAL.Migrations
{
    public partial class ForingKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PersonTask_Persons_PersonsId",
                table: "PersonTask");

            migrationBuilder.DropForeignKey(
                name: "FK_PersonTask_Tasks_TasksId",
                table: "PersonTask");

            migrationBuilder.RenameColumn(
                name: "TasksId",
                table: "PersonTask",
                newName: "TaskId");

            migrationBuilder.RenameColumn(
                name: "PersonsId",
                table: "PersonTask",
                newName: "PersonId");

            migrationBuilder.RenameIndex(
                name: "IX_PersonTask_TasksId",
                table: "PersonTask",
                newName: "IX_PersonTask_TaskId");

            migrationBuilder.AddColumn<int>(
                name: "OrderId",
                table: "Tasks",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_OrderId",
                table: "Tasks",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_ParentId",
                table: "Tasks",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_Cars_ClientId",
                table: "Cars",
                column: "ClientId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cars_Clients_ClientId",
                table: "Cars",
                column: "ClientId",
                principalTable: "Clients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PersonTask_Persons_PersonId",
                table: "PersonTask",
                column: "PersonId",
                principalTable: "Persons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PersonTask_Tasks_TaskId",
                table: "PersonTask",
                column: "TaskId",
                principalTable: "Tasks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Orders_OrderId",
                table: "Tasks",
                column: "OrderId",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Tasks_ParentId",
                table: "Tasks",
                column: "ParentId",
                principalTable: "Tasks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cars_Clients_ClientId",
                table: "Cars");

            migrationBuilder.DropForeignKey(
                name: "FK_PersonTask_Persons_PersonId",
                table: "PersonTask");

            migrationBuilder.DropForeignKey(
                name: "FK_PersonTask_Tasks_TaskId",
                table: "PersonTask");

            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Orders_OrderId",
                table: "Tasks");

            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Tasks_ParentId",
                table: "Tasks");

            migrationBuilder.DropIndex(
                name: "IX_Tasks_OrderId",
                table: "Tasks");

            migrationBuilder.DropIndex(
                name: "IX_Tasks_ParentId",
                table: "Tasks");

            migrationBuilder.DropIndex(
                name: "IX_Cars_ClientId",
                table: "Cars");

            migrationBuilder.DropColumn(
                name: "OrderId",
                table: "Tasks");

            migrationBuilder.RenameColumn(
                name: "TaskId",
                table: "PersonTask",
                newName: "TasksId");

            migrationBuilder.RenameColumn(
                name: "PersonId",
                table: "PersonTask",
                newName: "PersonsId");

            migrationBuilder.RenameIndex(
                name: "IX_PersonTask_TaskId",
                table: "PersonTask",
                newName: "IX_PersonTask_TasksId");

            migrationBuilder.AddForeignKey(
                name: "FK_PersonTask_Persons_PersonsId",
                table: "PersonTask",
                column: "PersonsId",
                principalTable: "Persons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PersonTask_Tasks_TasksId",
                table: "PersonTask",
                column: "TasksId",
                principalTable: "Tasks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
