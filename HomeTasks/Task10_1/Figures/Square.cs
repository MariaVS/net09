﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task10_1.Figures
{
    public class Square : Figure
    {
        public int Side { get; set; }
        public Square() { }

        public Square(int side)
        {
            Side = side;
        }

        public override void Draw()
        {
            for (int i = 0; i < Side; i++)
            {
                Console.Write("* ");
            }
            Console.WriteLine();
            for (int i = 1; i < Side - 1; i++)
            {
                Console.Write("*");
                Console.SetCursorPosition(2*(Side-1), Console.CursorTop);
                Console.WriteLine("*");
            }
            for (int i = 0; i < Side; i++)
            {
                Console.Write("* ");
            }
            Console.WriteLine();
        }
    }
}
