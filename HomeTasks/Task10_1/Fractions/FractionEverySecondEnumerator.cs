﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Task10_1.Fractions
{
    public class FractionEverySecondEnumerator : IEnumerator<Fraction>
    {
        public FractionEverySecondEnumerator(List<Fraction> list2)
        {
            Random rnd = new Random();
            _list = list2;
        }
        List<Fraction> _list;
        private int counter = -1;
        public Fraction Current => _list[counter];

        object IEnumerator.Current => _list[counter];

        public void Dispose()
        {
            _list.Clear();
        }

        public bool MoveNext()
        {
            counter += 2;
            if (counter < _list.Count)
            {
                return true;
            }

            return false;
        }

        public void Reset()
        {
            counter = -1;
        }
    }
}
