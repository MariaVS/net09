﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace AutoService.DAL.Serializer
{
    public static class XMLSerializer<T>
    {
        public static void Serialize(T instance, string fileRoute, Type objectType)
        {
            CheckOrCreateFile(fileRoute, objectType);
            var formatter = new XmlSerializer(typeof(T));
            using var fs = new FileStream(fileRoute, FileMode.Open);
            formatter.Serialize(fs, instance);
        }

        public static T Deserialize(string fileRoute, Type objectType)
        {
            CheckOrCreateFile(fileRoute, objectType);
            var formatter = new XmlSerializer(typeof(T));
            using var fs = new FileStream(fileRoute, FileMode.Open);
            return (T)formatter.Deserialize(fs);
        }

        private static void CheckOrCreateFile(string fileRoute, Type objectType)
        {
            if (!File.Exists(fileRoute))
            {
                using var stream = File.Create(fileRoute);
                using var sw = new StreamWriter(stream);
                sw.Write($"<ArrayOf{objectType.Name}>\n</ArrayOf{objectType.Name}>");
            }
        }
    }
}
