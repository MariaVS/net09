﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AutoService.DAL.Migrations
{
    public partial class UpdateAudit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Clients_Audits",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "New_DataOfUpd",
                table: "Clients_Audits",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "New_Email",
                table: "Clients_Audits",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "New_FirstName",
                table: "Clients_Audits",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "New_LastName",
                table: "Clients_Audits",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "New_Number",
                table: "Clients_Audits",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Email",
                table: "Clients_Audits");

            migrationBuilder.DropColumn(
                name: "New_DataOfUpd",
                table: "Clients_Audits");

            migrationBuilder.DropColumn(
                name: "New_Email",
                table: "Clients_Audits");

            migrationBuilder.DropColumn(
                name: "New_FirstName",
                table: "Clients_Audits");

            migrationBuilder.DropColumn(
                name: "New_LastName",
                table: "Clients_Audits");

            migrationBuilder.DropColumn(
                name: "New_Number",
                table: "Clients_Audits");
        }
    }
}
