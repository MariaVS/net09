﻿using AutoService.BLL.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AutoService.WebApplication.Controllers
{
    public class ClientsController : Controller
    {
        private readonly ILogger<ClientsController> _logger;
        private readonly IClientService _clientService;

        public ClientsController(ILogger<ClientsController> logger, IClientService clientService)
        {
            _clientService = clientService;
            _logger = logger;
        }

        public IActionResult Get()
        {
            var clients = _clientService.GetAllClientsWithCars();
            
            return View(clients);
        }
    }
}
