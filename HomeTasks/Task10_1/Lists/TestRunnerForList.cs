﻿using System;
using System.Collections.Generic;
using System.Text;
using Task10_1.Fractions;

namespace Task10_1.Lists
{
    class TestRunnerForList
    {
        public static void Test()
        {
            var myList = new LList<Fraction>(new Fraction(12, 15));
            myList.Add(new Fraction(12, 16));
            myList.Add(myList.Head, new Fraction(14, 17));
            var result = myList.FindReq(new Fraction(12, 15), myList.Head);
            Console.WriteLine(result?.Value);
            Console.WriteLine(myList.UpdateReq(new Fraction(12, 15), myList.Head, new Fraction(10, 11))?.Value);
        }
    }
}
