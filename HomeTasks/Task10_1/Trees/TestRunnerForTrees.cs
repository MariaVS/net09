﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task10_1.Trees
{
    public class TestRunnerForTrees
    {
        public static void Test ()
        {
            Tree tree = new Tree(8);
            tree.Add(3);
            tree.Add(6);
            tree.Add(1);
            tree.Add(4);
            tree.Add(7);
            tree.Add(10);
            tree.Add(14);
            tree.Add(13);
            tree.DrawToConsole();

        }
        
        
    }
}
