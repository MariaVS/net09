﻿using AutoService.DAL.Model;

namespace AutoService.DAL.Repositories
{
    public interface IUnitOfWork
    {
        PersonRepo PersonRepository { get; }
        CarRepository CarRepository { get; }
        TaskRepository TaskRepository { get; }
        GenericRepository<TaskArchive> TaskArchiveRepository { get; }
        GenericRepository<Client> ClientRepository { get; }
        IGenericRepository<T> GetGenericRepository<T>() where T : class;
    }
}
