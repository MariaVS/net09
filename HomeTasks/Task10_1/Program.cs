﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics.CodeAnalysis;
using Task10_1.Animals;
using Task10_1.Figures;
using Task10_1.Fractions;
using Task10_1.Horses;
using Task10_1.Lists;
using Task10_1.Tanks;
using Task10_1.Trees;

namespace Task10_1
{
    class Program
    {
        static void Main(string[] args)
        {
            TestRunnerForFraction.Test();
            //TestRunner.Test();
            //TestRunnerForFigure.Test();
            //TestRunnerForTank.Test();
            //TestRunnerForList.Test();
            //TestRunnerForTrees.Test();
            //TestRunnerForHorses.Test();
            Console.ReadLine();
        }
    }
}
