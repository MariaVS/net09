﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task10_1.Tanks
{
    public class TankRiseEnumerator : IEnumerator<Tank>
    {
        public TankRiseEnumerator(List<Tank> list)
        {
            _list = list.OrderBy(x => x.Armor + x.Damage).ToList();

        }

        private List<Tank> _list;
        private int counter = -1;
        public Tank Current => _list[counter];

        object IEnumerator.Current => _list[counter];

        public void Dispose()
        {
            _list.Clear();
        }

        public bool MoveNext()
        {
            counter++;
            if (counter < _list.Count)
            {
                return true;
            }

            return false;
        }

        public void Reset()
        {
            counter = -1;
        }
    }
}
