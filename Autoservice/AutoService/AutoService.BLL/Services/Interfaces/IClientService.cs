﻿using System.Collections.Generic;
using AutoService.DAL.Model;

namespace AutoService.BLL.Services.Interfaces
{
    public interface IClientService
    {
        List<Client> GetAllClientsWithCars();
    }
}
