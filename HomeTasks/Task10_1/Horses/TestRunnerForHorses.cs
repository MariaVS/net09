﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Task10_1.Horses
{
    public class TestRunnerForHorses
    {
        public static void Test()
        {
            Horse horse = new Horse();
            SlowRun slowRun = new SlowRun();
            FastRun fastRun = new FastRun();
            horse.RunType = fastRun;
            horse.Run();
            Thread.Sleep(5000);
            horse.RunType = slowRun;
            horse.Run();
        }
    }
}
