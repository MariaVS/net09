﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task10_1.Tanks
{
    public class TankRandomEnumerator : IEnumerator<Tank>
    {
        public TankRandomEnumerator(List<Tank> list2)
        {
            Random rnd = new Random();
            _list = list2.OrderBy(x => rnd.Next()).ToList();
        }
        List<Tank> _list;
        private int counter = -1;
        public Tank Current => _list[counter];

        object IEnumerator.Current => _list[counter];

        public void Dispose()
        {
            _list.Clear();
        }

        public bool MoveNext()
        {
            counter++;
            if (counter < _list.Count)
            {
                return true;
            }

            return false;
        }

        public void Reset()
        {
            counter = -1;
        }
    }
}
