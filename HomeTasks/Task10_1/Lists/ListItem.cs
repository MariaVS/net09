﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task10_1.Lists
{
    class ListItem<T>
    {
        public T Value { get; set; }
        public ListItem<T> Next { get; set; }
        public ListItem(T value)
        {
            this.Value = value;
        }
    }
}
