﻿using System.Linq;
using AutoService.DAL.Model;

namespace AutoService.DAL.Repositories.Interface
{
    public interface ICarsRepository : IGenericRepository<Car>
    {
        IQueryable<Car> ReadAllByClientId(int clientId);
    }
}
