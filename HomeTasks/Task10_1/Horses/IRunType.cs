﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task10_1.Horses
{
    public interface IRunType
    {
        void Run();
    }
}
