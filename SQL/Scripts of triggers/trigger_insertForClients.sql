use [TestBase]
go
CREATE OR ALTER TRIGGER [dbo].[Clients_INSERT] 
   ON [Clients] 
   AFTER INSERT
AS
begin
  UPDATE Clients SET DataOfUpd = getdate() 
  WHERE Id IN (select Id from inserted)
end
