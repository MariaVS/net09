﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task10_1.Figures
{
    public class TestRunnerForFigure
    {
        public static void Test()
        {

            List<Figure> figure = new List<Figure>() {new Rhombus(4, 5), new Square(4), new Rectangle(3, 5),  new Circle(3, 3, 3) };
            foreach (var fig in figure)
            {
                fig.Draw();
                Console.WriteLine();
            }

        }
    }
}
