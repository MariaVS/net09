﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoService.BLL.Services.Interfaces;
using AutoService.DAL.Model;
using AutoService.DAL.Repositories.Interface;

namespace AutoService.BLL.Services
{
    public class ClientXmlService : IClientXmlService
    {
        private IClientXmlRepository _clientXmlRepository;
        public ClientXmlService(IClientXmlRepository clientXmlRepository)
        {
            _clientXmlRepository = clientXmlRepository;
        }

        public void Add(Client client, string fileRoute)
        {
            var clients = _clientXmlRepository.ReadAll(fileRoute);
            clients.Add(client);
            _clientXmlRepository.WrightAll(clients, fileRoute);
        }

        public List<Client> ReadAll(string fileRoute)
        {
            return _clientXmlRepository.ReadAll(fileRoute);
        }
    }
}
