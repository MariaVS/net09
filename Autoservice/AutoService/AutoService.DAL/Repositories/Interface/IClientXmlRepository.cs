﻿using AutoService.DAL.Model;
using System.Collections.Generic;

namespace AutoService.DAL.Repositories.Interface
{
    public interface IClientXmlRepository
    {
        void WrightAll(List<Client> clients, string fileRoute);
        List<Client> ReadAll(string fileRoute);
    }
}
