﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Task10_1.Fractions
{
    class FractionValueComparer : IComparer<Fraction>
    {
        public int Compare( Fraction x, Fraction y)
        {
            return x.CompareTo(y);
        }
    }
}
