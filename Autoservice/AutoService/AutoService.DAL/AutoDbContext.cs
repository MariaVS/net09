﻿using System.Diagnostics;
using System.IO;
using AutoService.DAL.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace AutoService.DAL
{
    /// <inheritdoc />
   public class AutoDbContext : DbContext
   {
       //Added only for Add-Migration call
       public AutoDbContext() : this(GetOptions())
       {
           
       }

        public AutoDbContext(DbContextOptions options) : base(options)
        {
            
        }

        public DbSet<Person> Persons { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<TaskPriority> TaskPriorities { get; set; }
        public DbSet<Clients_Audit> Clients_Audits { get; set; }
        public DbSet<TaskArchive> TaskArchives { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<TaskPriority>().HasData(
                new TaskPriority { Id = 1, Priority = "Minor"},
                new TaskPriority { Id = 2, Priority = "Major"},
                new TaskPriority { Id = 3, Priority = "Critical"});
        }

        //use only for Add-Migration
        private static DbContextOptions GetOptions()
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();
            string connectionString = config.GetConnectionString("DefaultConnection");

            var optionsBuilder = new DbContextOptionsBuilder<AutoDbContext>();
            optionsBuilder.LogTo(message => Debug.WriteLine(message));
            var options = optionsBuilder.UseSqlServer(connectionString).Options;

            return options;
        }
   }
}
