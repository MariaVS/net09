﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Task10_1.Fractions
{
    public class TestRunnerForFraction
    {
        public static void Test()
        {
            //Console.WriteLine("Hello World!");
            //Fraction fr = new Fraction(1, 2);
            //Console.WriteLine(fr.GetHashCode());
            //Fraction fr2 = new Fraction(2, 4);
            //Console.WriteLine(fr2.GetHashCode());
            //Console.WriteLine(fr.Equals(fr2));
            //Fraction fr3 = new Fraction(15, 18);
            //Fraction fr4 = new Fraction(20, 25);
            //Fraction fr5 = new Fraction(26, 49);
            //Fraction fr6 = new Fraction(7, 5);
            //Fraction fr7 = new Fraction(8, 23);
            //Dictionary<int, Fraction> dic = new Dictionary<int, Fraction>
            //{
            //    [1] = new Fraction(12, 14),
            //    [2] = new Fraction(15, 18),
            //    [3] = new Fraction(24, 24),
            //    [4] = new Fraction(30, 36),
            //    [5] = new Fraction(32, 56)

            //};
            //Console.WriteLine(dic.ContainsValue(fr3));
            //Console.WriteLine(fr3.CompareTo(dic[3]));
            //Fraction[] fractions = { fr, fr3, fr2 };
            //Console.WriteLine("Before sort");
            //foreach (var f in fractions)
            //{
            //    Console.WriteLine(f);
            //}
            //Array.Sort(fractions, new FractionAlphabeticalComparer());
            //Console.WriteLine("After sort");
            //foreach (var f in fractions)
            //{
            //    Console.WriteLine(f);
            //}
            //Fraction[] fractions2 = { fr, fr3, fr2 };
            //Console.WriteLine("Before the second sort");
            //foreach (var f in fractions2)
            //{
            //    Console.WriteLine(f);
            //}
            //Array.Sort(fractions2, new FractionValueComparer());
            //Console.WriteLine("After the second sort");
            //foreach (var f in fractions)
            //{
            //    Console.WriteLine(f);
            //}

            //var fractions3 = new List<Fraction> { fr, fr2, fr3, fr4, fr5, fr6, fr7 };
            //var fractionRandomEnumerator = new FractionRandomEnumerator(fractions3);
            //Console.WriteLine("Before FractionRandomEnumerator");
            //foreach (var f in fractions3)
            //{
            //    Console.WriteLine(f);
            //}
            //Console.WriteLine("On FractionRandomEnumerator");
            //while (fractionRandomEnumerator.MoveNext())
            //{
            //    Console.WriteLine(fractionRandomEnumerator.Current);
            //}

            //var fractionEverySecondEnumerator = new FractionEverySecondEnumerator(fractions3);
            //Console.WriteLine("Before FractionEverySecondEnumerator");
            //foreach (var f in fractions3)
            //{
            //    Console.WriteLine(f);
            //}
            //Console.WriteLine("On FractionEverySecondEnumerator");
            //while (fractionEverySecondEnumerator.MoveNext())
            //{
            //    Console.WriteLine(fractionEverySecondEnumerator.Current);
            //}
            
            var fraction = new Fraction(1,2);
            fraction.Onchange += Change;
            var fraction2 = new Fraction(3, 2);
            fraction2.Onchange += Change2;
            fraction.Num = 7;
            fraction2.Num = 9;
            void Change(int p)
            {
                Console.WriteLine($"You change {p}");
            }

            void Change2(int p)
            {
                Debug.WriteLine($"You change {p}");
            }
        }

    }
}
