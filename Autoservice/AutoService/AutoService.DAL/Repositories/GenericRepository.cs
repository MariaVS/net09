﻿using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace AutoService.DAL.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        protected readonly AutoDbContext Context;

        public GenericRepository(AutoDbContext context)
        {
            Context = context;
        }

        public virtual T Read(int id)
        {
            return Context.Find<T>(id);
        }

        public virtual T Update(T entity)
        {
            var result = Context.Update<T>(entity).Entity;
            Context.SaveChanges();
            return result;
        }

        public virtual IQueryable<T> ReadAll()
        {
            return Context.Set<T>();
        }

        public virtual void Delete(T entity)
        {
            Context.Remove<T>(entity);
            Context.SaveChanges();
        }

        public virtual T Add(T entity)
        {
            var result = Context.Add(entity).Entity;
            Context.SaveChanges();
            return result;

        }
    }
}
