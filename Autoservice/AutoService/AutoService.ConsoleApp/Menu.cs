﻿using AutoService.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using AutoService.DAL.Repositories;

namespace AutoService.ConsoleApp
{
    public class Menu
    {
        private readonly AutoServiceMenuRepository _autoServiceMenuRepository;
        public Menu(AutoServiceMenuRepository autoService)
        {
            _autoServiceMenuRepository = autoService;
        }

        public void ShowMainMenu()
        {
            do
            {
                Console.WriteLine("Выберете число от 0 до 2, где: \n1-Добавить клиента; \n2-Найти клиена; \n0-Выйти;");
                string number = Console.ReadLine();
                switch (number)
                {
                    case "1":
                        CreateClientAndCar();
                        break;
                    case "2":
                        ShowSearchMenu();
                        break;
                    case "0":
                        return;
                    default:
                        Console.WriteLine("Вы ввели неверное число");
                        break;
                }
            } while (true);
        }

        private void CreateClientAndCar()
        {
            Console.WriteLine("Заполнение клиента");
            Console.WriteLine($"Введите {nameof(Client.FirstName)}: ");
            string firstName = Console.ReadLine();
            Console.WriteLine($"Введите {nameof(Client.LastName)}: ");
            string lastName = Console.ReadLine();
            Console.WriteLine($"Введите {nameof(Client.Number)}: ");
            string numberClient = Console.ReadLine();
            Console.WriteLine($"Введите {nameof(Client.Email)}: ");
            string email = Console.ReadLine();
            var client = _autoServiceMenuRepository.CreateClient(firstName, lastName, numberClient, email);

            Console.WriteLine("Заполнение машины");
            Console.WriteLine($"Введите {nameof(Car.Model)}: ");
            string model = Console.ReadLine();
            Console.WriteLine($"Введите {nameof(Car.Manufacture)}: ");
            string manufacture = Console.ReadLine();
            Console.WriteLine($"Введите {nameof(Car.VIN)}: ");
            string vin = Console.ReadLine();
            Console.WriteLine($"Введите {nameof(Car.Number)}: ");
            string numberCar = Console.ReadLine();
            _autoServiceMenuRepository.CreateCar(model, manufacture, vin, numberCar, client.Id);
        }

        private void ShowSearchMenu()
        {
            Console.WriteLine("Выберите вариант поиска от 0 до 2, где: \n1-Поиск по Id; \n2-Поиск по фамилии; \n0-Назад;");
            string number = Console.ReadLine();
            switch (number)
            {
                case "1":
                    ShowShortNameById();
                    ShowSearchMenu();
                    break;
                case "2":
                    ShowShortNamesByLastName();
                    ShowSearchMenu();
                    break;
                case "0":
                    return;
                default:
                    Console.WriteLine("Вы ввели неверное число");
                    ShowSearchMenu();
                    break;
            }

        }

        private void ShowShortNameById()
        {
            Console.WriteLine("Введите пожалуйста Id клиента");
            string strclientId = Console.ReadLine();
            if (int.TryParse(strclientId, out int clientId))
            {
                var client = _autoServiceMenuRepository.SearchClientById(clientId);
                if (client != null)
                {
                    Console.WriteLine($"Результаты поиска для Id: {clientId}");
                    ShowListClientsMenu(new List<Client> { client });
                }
                else
                {
                    Console.WriteLine($"Клиент по Id: {clientId} не найден");
                }
            }
            else
            {
                Console.WriteLine("Id должно быть числом");
            }
        }

        private void ShowShortNamesByLastName()
        {
            Console.WriteLine("Введите пожалуйста фамилию клиента");
            string lastName = Console.ReadLine();
            var clients = _autoServiceMenuRepository.SearchClientByLastName(lastName);
            if (clients.Any())
            {
                Console.WriteLine($"Результаты поиска для фамилии: {lastName}");
                ShowListClientsMenu(clients);
            }
            else
            {
                Console.WriteLine($"Клиенты по фамилии: {lastName} не найдены");
            }
        }

        private void ShowListClientsMenu(IList<Client> clients)
        {
            int counter = 0;
            foreach (var client in clients)
            {
                Console.WriteLine($"{++counter}. {client.LastName} {client.FirstName}");
            }
            Console.WriteLine("0-Назад");
            string number = Console.ReadLine();
            switch (number)
            {
                case "0":
                    return;
                default:
                    if (int.TryParse(number, out int clientNumber))
                    {
                        if (clientNumber > 0 && clientNumber <= clients.Count)
                        {
                            ShowClientMenu(clients[clientNumber - 1].Id);
                            ShowListClientsMenu(clients);
                        }
                        else
                        {
                            Console.WriteLine($"Клиент по номеру: {clientNumber} не найден");
                            ShowListClientsMenu(clients);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Номер должен быть числом");
                        ShowListClientsMenu(clients);
                    }
                    break;
            }
        }

        private void ShowClientMenu(int clientId)
        {
            Console.WriteLine("Выберете действие для клиента от 0 до 3, где: \n1-Подробная информация; \n2-Редактировать; \n3-Удалить; \n0-Назад;");
            string number = Console.ReadLine();
            switch (number)
            {
                case "1":
                    ShowDetailedClient(clientId);
                    ShowClientMenu(clientId);
                    break;
                case "2":
                    UpdateClient(clientId);
                    ShowClientMenu(clientId);
                    break;
                case "3":
                    RemoveClient(clientId);
                    break;
                case "0":
                    return;
                default:
                    Console.WriteLine("Вы ввели неверное число");
                    ShowClientMenu(clientId);
                    break;
            }
        }

        private void ShowDetailedClient(int clientId)
        {
            var client = _autoServiceMenuRepository.SearchClientById(clientId);
            Console.WriteLine($"-----клиент-----");
            Console.WriteLine($"{nameof(client.Id)}: {client.Id}");
            Console.WriteLine($"{nameof(client.FirstName)}: {client.FirstName}");
            Console.WriteLine($"{nameof(client.LastName)}: {client.LastName}");
            Console.WriteLine($"{nameof(client.Number)}: {client.Number}");
            Console.WriteLine($"{nameof(client.Email)}: {client.Email}");
            Console.WriteLine($"\t-----машины-----");
            foreach (var car in client.Cars.ToList())
            {
                Console.WriteLine($"\t{nameof(car.Id)}: {car.Id}");
                Console.WriteLine($"\t{nameof(car.Manufacture)}: {car.Manufacture}");
                Console.WriteLine($"\t{nameof(car.Model)}: {car.Model}");
                Console.WriteLine($"\t{nameof(car.Number)}: {car.Number}");
                Console.WriteLine($"\t{nameof(car.VIN)}: {car.VIN}");
                Console.WriteLine($"\t***");
            }

            Console.WriteLine($"\t-----заказы-----");
            foreach (var order in client.Orders.ToList())
            {
                Console.WriteLine($"\t{nameof(order.Id)}: {order.Id}");
                Console.WriteLine($"\t\t-----задачи-----");
                foreach (var task in order.Tasks.ToList())
                {
                    Console.WriteLine($"\t\t{nameof(task.Id)}: {task.Id}");
                    Console.WriteLine($"\t\t{nameof(task.Description)}: {task.Description}");
                    Console.WriteLine($"\t\t{nameof(task.IsCompleted)}: {task.IsCompleted}");
                    Console.WriteLine($"\t\t{nameof(task.IsInWork)}: {task.IsInWork}");
                    Console.WriteLine($"\t\t{nameof(task.IsPending)}: {task.IsPending}");
                    Console.WriteLine($"\t\t{nameof(task.Title)}: {task.Title}");
                    Console.WriteLine($"\t\t***");
                }
                Console.WriteLine($"\t***");
            }
        }

        private void UpdateClient(int clientId)
        {
            var client = _autoServiceMenuRepository.SearchClientById(clientId);
            Console.WriteLine($"-----клиент-----");
            Console.WriteLine($"{nameof(client.FirstName)}: {client.FirstName}");
            Console.WriteLine($"Введите {nameof(Client.FirstName)}: ");
            string firstName = Console.ReadLine();
            Console.WriteLine($"{nameof(client.LastName)}: {client.LastName}");
            Console.WriteLine($"Введите {nameof(Client.LastName)}: ");
            string lastName = Console.ReadLine();
            Console.WriteLine($"{nameof(client.Number)}: {client.Number}");
            Console.WriteLine($"Введите {nameof(Client.Number)}: ");
            string numberClient = Console.ReadLine();
            Console.WriteLine($"{nameof(client.Email)}: {client.Email}");
            Console.WriteLine($"Введите {nameof(Client.Email)}: ");
            string email = Console.ReadLine();
            client.FirstName = firstName;
            client.LastName = lastName;
            client.Number = numberClient;
            client.Email = email;
            _autoServiceMenuRepository.UpdateClient(client);
        }

        private void RemoveClient(int clientId)
        {
            var client = _autoServiceMenuRepository.SearchClientById(clientId);
            Console.WriteLine($"Вы действительно хотите удалить клиента: {client.LastName} {client.FirstName}");
            Console.WriteLine("Y-Удалить; N-Отменить;");
            string number = Console.ReadLine();
            if (number == "Y" || number == "y")
            {
                _autoServiceMenuRepository.RemoveClient(client);
            }
        }
    }
}
