﻿using AutoService.DAL.Model;

namespace AutoService.DAL.Repositories
{
    public interface IPersonRepository : IGenericRepository<Person>
    {
        void Delete(int id);
    }
}
