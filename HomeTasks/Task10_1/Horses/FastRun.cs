﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task10_1.Horses
{
    public class FastRun : IRunType
    {
        public void Run()
        {
            Console.WriteLine("I run fast");
        }
    }
}
