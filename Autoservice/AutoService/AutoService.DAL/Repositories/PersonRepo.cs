﻿using AutoService.DAL.Model;

namespace AutoService.DAL.Repositories
{
    public class PersonRepo: GenericRepository<Person>, IPersonRepository
    {
        public PersonRepo(AutoDbContext context) : base(context) {}

        public override void Delete(Person person)
        {
            Context.Remove(person);
            Context.SaveChanges();
        }

        public void Delete(int id)
        {
            var person = Context.Find<Person>(id);
            Delete(person);
        }
    }
}
