﻿namespace Task10_1.Trees
{
    public class TreeItemLevel<T>
    {
        public TreeItem<T> TreeItem { get; set; }
        public int Level { get; set; }
    } 
}
