﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoService.DAL.Model
{
    public class Clients_Audit
    {
        public DateTime DataOfUpd { get; set; }
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Number { get; set; }
        public string Email { get; set; }
        public DateTime New_DataOfUpd { get; set; }
        public string New_LastName { get; set; }
        public string New_FirstName { get; set; }
        public string New_Number { get; set; }
        public string New_Email { get; set; }
    }
}
