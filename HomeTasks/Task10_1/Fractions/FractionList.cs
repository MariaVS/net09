﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Task10_1.Fractions
{
    public class FractionList : IEnumerable<Fraction>
    {
        public List<Fraction> _list = new List<Fraction>();

        public FractionList(List<Fraction> list)
        {
            _list = list;
        }

        public IEnumerator<Fraction> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _list.GetEnumerator();
        }
    }
}
