﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoService.DAL.Model;

namespace AutoService.BLL.Services.Interfaces
{
    public interface IClientXmlService
    {
        void Add(Client client, string fileRoute);
        List<Client> ReadAll(string fileRoute);
    }
}
