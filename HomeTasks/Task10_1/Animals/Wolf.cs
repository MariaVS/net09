﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task10_1.Animals
{
    class Wolf : Herd
    {
        public new void Say()
        {
            Console.WriteLine($"I am {GetType().Name}");
        }
    }
}
