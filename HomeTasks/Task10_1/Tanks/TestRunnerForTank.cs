﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Task10_1.Tanks
{
    public class TestRunnerForTank
    {
        public static void Test()
        {
            Tank tankOneForFirstArmy = new Tank() { Name = "T13", Armor = 90, Damage = 80};
            Tank tankTwoForFirstArmy = new Tank() { Name = "T8", Armor = 80, Damage = 100};
            Tank tankThreeForFirstArmy = new Tank() { Name = "Mouse", Armor = 70, Damage = 50};
            Tank tankFourForFirstArmy = new Tank() { Name = "Tiger", Armor = 60, Damage = 80 };
            Tank tankFiveForFirstArmy = new Tank() { Name = "T34", Armor = 77, Damage = 99 };

            Tank tankOneForSecondArmy = new Tank() { Name = "Tiger", Armor = 70, Damage = 88 };
            Tank tankTwoForSecondArmy = new Tank() { Name = "T32", Armor = 59, Damage = 86 };
            Tank tankThreeForSecondArmy = new Tank() { Name = "T15", Armor = 87, Damage = 54 };
            Tank tankFourForSecondArmy = new Tank() { Name = "Trendil", Armor = 67, Damage = 76 };
            Tank tankFiveForSecondArmy = new Tank() { Name = "Winner", Armor = 45, Damage = 81 };

            List<Tank> lisrArmyOne = new List<Tank>() { tankOneForFirstArmy, tankTwoForFirstArmy, tankThreeForFirstArmy, tankFourForFirstArmy, tankFiveForFirstArmy};
            List<Tank> lisrArmyTwo = new List<Tank>() { tankOneForSecondArmy, tankTwoForSecondArmy, tankThreeForSecondArmy, tankFourForSecondArmy, tankFiveForSecondArmy };

            Army firstArmy = new Army(lisrArmyOne, EnumForEnumerator.RandomEnumerator);
            Army secondArmy = new Army(lisrArmyTwo, EnumForEnumerator.RiseEnumerator);
            foreach (var tankOne in firstArmy)
            { 
                foreach (var tankTwo in secondArmy)
                {
                    Console.WriteLine($"{tankOne.Name} \t {tankTwo.Name}");
                    Tank.Multiply(tankOne, tankTwo);
                    Console.WriteLine($"{tankOne.Wins} \t {tankTwo.Wins}");
                }
                Console.WriteLine("****************");
            }

            var winsFirstArmy = firstArmy.Sum(x => x.Wins);
            var winsSecondArmy = secondArmy.Sum(x => x.Wins);
            Console.WriteLine( $"Wins: {winsFirstArmy}    {winsSecondArmy}");
            if (winsFirstArmy > winsSecondArmy) 
            {
                Console.WriteLine("First army won");
            }
            else if (winsFirstArmy == winsSecondArmy)
            {
                Console.WriteLine("Draw");
            }
            else
            {
                Console.WriteLine("Second army won");
            }
            
        }
    }
}
